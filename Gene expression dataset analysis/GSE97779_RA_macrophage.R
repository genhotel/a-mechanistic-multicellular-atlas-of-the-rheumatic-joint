setwd("C:/Users/I0471594/OneDrive - Sanofi/PhD/Macrophage_map/Macrophage_expression_datasets_analysis/GSE97779_RA_VS_C_macrophage_human_array_dataset")
library(openxlsx)
library(preprocessCore)
library(biomaRt)
library(AnnotationHub)
library(plyr)
library(dplyr)
library(limma)
library(nortest)
#read the expression matrix
expression_matrix= read.delim("GSE97779_series_matrix.txt", comment.char="!",row.names=1)

#quantile normalization 
norm_expression_matrix=normalize.quantiles(as.matrix(expression_matrix))
#restore rows' and columns' names after normalization 
colnames(norm_expression_matrix)=c("Ctrl","Ctrl","Ctrl","Ctrl","Ctrl","RA","RA","RA","RA","RA","RA","RA","RA","RA")
rownames(norm_expression_matrix) = rownames(expression_matrix)

#test the normality and variance of Ctrl and RA populations: 
Ctrl_samples= norm_expression_matrix[,c(1:5)]
RA_samples = norm_expression_matrix[,c(6:ncol(norm_expression_matrix))]
hist(unlist(log2(Ctrl_samples)), breaks=100,main="Gene expression distribution in control samples")
hist(unlist(log2(RA_samples)), breaks=100,main="Gene expression distribution in RA samples")
# with Anderson-Darling test for normality :P-value < 0.05 = not normal 
ad.test(Ctrl_samples)
ad.test(RA_samples)


# assign samples to groups and set up design matrix
gs <- factor(colnames(norm_expression_matrix))
groups <- make.names(c("control","RA"))
levels(gs) <- groups
norm_expression_matrix=as.data.frame(norm_expression_matrix)
norm_expression_matrix_t=t(norm_expression_matrix)
norm_expression_matrix_t=as.data.frame(norm_expression_matrix_t)

norm_expression_matrix_t$group<- gs
design <- model.matrix(~group + 0, norm_expression_matrix_t)
colnames(design) <- levels(gs)

fit <- lmFit(norm_expression_matrix_t, design)  # fit linear model

# set up contrasts of interest and recalculate model coefficients
cts <- paste(groups[2], groups[1], sep="-")
cont.matrix <- makeContrasts(contrasts=cts, levels=design)
fit2 <- contrasts.fit(fit, cont.matrix)

# compute statistics and table of top significant genes
fit2 <- eBayes(fit2, 0.01)
tT <- topTable(fit2, adjust="fdr", sort.by="B", number=10000000)

tT <- subset(tT, select=c("ID","adj.P.Val","P.Value","t","B","logFC"))

#annotate the affy probes with biomart

ensembl = useEnsembl(biomart = "ensembl", dataset = "hsapiens_gene_ensembl", mirror = "useast")
annotation= getBM(
  mart=ensembl,
  attributes=c(
    "affy_hg_u133_plus_2",
    "ensembl_gene_id",
    "hgnc_symbol"),
  filters = "affy_hg_u133_plus_2",
  values = rownames(averaged_norm_expression_matrix),uniqueRows=TRUE)
colnames(annotation)[1]="ID"

#join annotation file to the expression matrix

annotated_DE_genes=join(annotation,tT,type="right",by="ID") 

#Check the non annotated probes : 
non_annotated_DE_genes=annotated_DE_genes[annotated_DE_genes$ensembl_gene_id=="",]


#probes with ensembl ID but no HGNC ID: we have Uncharacterized proteins, lnc RNAs, snoRNA, anti sens RNAs,...
#Two probes can capture the same gene.
#One probe can capture different genes (one affy ID for many ensembl/HGNC IDs)


