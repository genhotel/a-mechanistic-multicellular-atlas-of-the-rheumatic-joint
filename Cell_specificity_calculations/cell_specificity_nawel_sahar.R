#import packages

library(dplyr)
library(tidyr)
library(xlsx)


## FOR NAWEL'S CELL SPECIFIC MAPS 

# 1) MACROPHAGES

# Load the species csv file from celldesigner including the name and the class column
species <- read.csv("species_macro.csv", sep=",")

#remove degraded components:
species=species[!species$class=="DEGRADED",]

# correct the species names
species$name = gsub("_slash_","/",
                          gsub("_space_", " ",
                          gsub("_plus_", "+",
                          gsub("_sub_", "",
                          gsub("_endsub_", "",
                          gsub("_minus_","-",
                          gsub ("_underscore_","_",species$name)))))))

#separate the molecular complexes into their components 

species <-tidyr::separate(
    data =species,
    col = name,
    sep = "/",
    into = c("A","B","C","D","E","F","G"),
    remove = T
  )


#keep the names only, concatenate the complex components 

species_name_only=c(species$A,species$B,
                    species$C,species$D,
                    species$E,species$F,
                    species$G)

species_name_only=species_name_only[!is.na(species_name_only)]


# Remove duplicate rows
unique_species= unique(species_name_only)

# Remove the phenotypes (not considered in the overlay calculations)

pheno_macro=c("angiogenesis","inflammation",
              "Matrix degradation","Cell chemotaxis_migration","apoptosis",
              "T cells activation",
              "proliferation_survival","osteoclastogenesis")

unique_species_macro=unique_species[!unique_species %in% pheno_macro]


# Import the overlay files
macro_overlay = read.csv("macrophages.csv", sep=";", header=TRUE)


# Calculate the percentage of the map which is cell-type specific
macro_spe = round(((sum(tolower(unique_species_macro) %in% tolower(macro_overlay$name), na.rm = TRUE))/length(unique_species))*100)



# 2) FIBROBLAST

# Load the species csv file from celldesigner including the name and the class column
species <- read.csv("species_fibro.csv", sep=",")

#remove degraded components:
species=species[!species$class=="DEGRADED",]

# correct the species names
species$name = gsub("_slash_","/",
                    gsub("_space_", " ",
                         gsub("_plus_", "+",
                              gsub("_sub_", "",
                                   gsub("_endsub_", "",
                                        gsub("_minus_","-",
                                             gsub ("_underscore_","_",species$name)))))))

#separate the molecular complexes into their components 

species <-tidyr::separate(
  data =species,
  col = name,
  sep = "/",
  into = c("A","B","C","D","E","F","G"),
  remove = T
)


#keep the names only, concatenate the complex components 

species_name_only=c(species$A,species$B,
                    species$C,species$D,
                    species$E,species$F,
                    species$G)

species_name_only=species_name_only[!is.na(species_name_only)]


# Remove duplicate rows
unique_species= unique(species_name_only)

# Remove the phenotypes (not considered in the overlay calculations)

pheno_fibro=c("Angiogenesis","Bone Erosion","Apoptosis","Matrix Degradation","Osteoclastogenesis",
              "Inflammation","Cell Chemotaxis","Recruitment","Infiltration",
              "Cell growth","Survival","Proliferation")

unique_species_fibro=unique_species[!unique_species %in% pheno_fibro]



# Import the overlay files
RASF_overlay = read.csv("RASF_overlay_complet_colors_minerva.csv", sep=";", header=TRUE) 



# Calculate the percentage of the map which is cell-type specific
fibro_spe = round(((sum(tolower(unique_species_fibro) %in% tolower(RASF_overlay$name), na.rm = TRUE))/length(unique_species))*100)


# 3) TH1

# Load the species csv file from celldesigner including the name and the class column
species_macro <- read.csv("species_th1.csv", sep=",")

#remove degraded components:
species=species[!species$class=="DEGRADED",]

# correct the species names
species$name = gsub("_slash_","/",
                    gsub("_space_", " ",
                         gsub("_plus_", "+",
                              gsub("_sub_", "",
                                   gsub("_endsub_", "",
                                        gsub("_minus_","-",
                                             gsub ("_underscore_","_",species$name)))))))

#separate the molecular complexes into their components 

species <-tidyr::separate(
  data =species,
  col = name,
  sep = "/",
  into = c("A","B","C","D","E","F","G"),
  remove = T
)


#keep the names only, concatenate the complex components 

species_name_only=c(species$A,species$B,
                    species$C,species$D,
                    species$E,species$F,
                    species$G)

species_name_only=species_name_only[!is.na(species_name_only)]


# Remove duplicate rows
unique_species= unique(species_name_only)

# Remove the phenotypes (not considered in the overlay calculations)


pheno_TH1=c("angiogenesis","apoptosis","osteoclastogenesis",
            "inflammation","chemotaxis","proliferation")


unique_species_th1=unique_species[!unique_species %in% pheno_TH1]



# Import the overlay files

TH1_overlay = read.csv("TH1.csv", sep=";", header=TRUE)


# Calculate the percentage of the map which is cell-type specific

th1_spe = round(((sum(tolower(unique_species_th1) %in% tolower(TH1_overlay$name), na.rm = TRUE))/length(unique_species))*100)















## FOR SAHAR'S RA-MAP V2

# Load the species csv file from celldesigner including the name and the class column
species <- read.csv("species_ra_map_v2.csv", sep=",")

#remove degraded components:
species=species[!species$class=="DEGRADED",]

# correct the species names
species$name = gsub("_slash_","/",
                    gsub("_space_", " ",
                         gsub("_plus_", "+",
                              gsub("_sub_", "",
                                   gsub("_endsub_", "",
                                        gsub("_minus_","-",
                                             gsub ("_underscore_","_",species$name)))))))

#separate the molecular complexes into their components 

species <-tidyr::separate(
  data =species,
  col = name,
  sep = "/",
  into = c("A","B","C","D","E","F","G"),
  remove = T
)


#keep the names only, concatenate the complex components 

species_name_only=c(species$A,species$B,
                    species$C,species$D,
                    species$E,species$F,
                    species$G)

species_name_only=species_name_only[!is.na(species_name_only)]


# Remove duplicate rows
unique_species= unique(species_name_only)

# Remove the phenotypes (not considered in the overlay calculations)


pheno_RA_map_V2=c("Angiogenesis","Bone Erosion","Apoptosis","Matrix Degradation","Osteoclastogenesis",
                   "Inflammation","Cell Chemotaxis","Recruitment","Infiltration",
                   "Cell growth","Survival","Proliferation", "Hypoxia")

unique_species=unique_species[!unique_species %in% pheno_RA_map_V2]


# Import the overlay files
RASF_overlay = read.csv("RASF.csv", sep=";", header=TRUE) 
macro_overlay = read.csv("macrophages.csv", sep=";", header=TRUE)
PBMC_overlay = read.csv("PBMC.csv", sep=";", header=TRUE)
chondrocytes_overlay = read.csv("chondrocytes.csv", sep=";", header=TRUE) 
serum_overlay = read.csv("serum.csv", sep=";", header=TRUE)
synovial_fluid_overlay = read.csv("synovial_fluid.csv", sep=";", header=TRUE)
synovial_tissue_overlay = read.csv("synovial_tissue.csv", sep=";", header=TRUE)
blood_overlay = read.csv("blood_components.csv", sep=";", header=TRUE)


# Calculate the percentage of the map which is cell-type specific
RASF = round(((sum(tolower(unique_species) %in% tolower(RASF_overlay$name), na.rm = TRUE))/length(unique_species))*100)
synovial_tissue = round(((sum(tolower(unique_species) %in% tolower(synovial_tissue_overlay$name), na.rm = TRUE))/length(unique_species))*100)
Synovial_fluid = round(((sum(tolower(unique_species) %in% tolower(synovial_fluid_overlay$name), na.rm = TRUE))/length(unique_species))*100)
PBMC = round(((sum(tolower(unique_species) %in% tolower(PBMC_overlay$name), na.rm = TRUE))/length(unique_species))*100)
Blood = round(((sum(tolower(unique_species) %in% tolower(blood_overlay$name), na.rm = TRUE))/length(unique_species))*100)
Chondrocytes = round(((sum(tolower(unique_species) %in% tolower(chondrocytes_overlay$name), na.rm = TRUE))/length(unique_species))*100)
Macrophages = round(((sum(tolower(unique_species) %in% tolower(macro_overlay$name), na.rm = TRUE))/length(unique_species))*100)
Serum = round(((sum(tolower(unique_species) %in% tolower(serum_overlay$name), na.rm = TRUE))/length(unique_species))*100)

