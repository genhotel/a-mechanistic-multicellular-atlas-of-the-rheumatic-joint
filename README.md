# A Mechanistic Multicellular Atlas of the Rheumatic Joint

You can find in this repository: 


- Cell_specificity_calculations:

All the cell-specific XML files of the maps;

The overlay files containing cell-specific genes (extracted from literature and publicly available datasets);

R script to calculate the cellular specificity of each cell-specific map.


- Confidence_score_overlay:

R script used to calculate an annotation score for each compound present in the RA-map V2 based on the number of bibliographic references describing it;

The CSV file we extracted from CellDesigner containing all components of the RA-map V2 and its associated MIRIAM annotions.


- Gene expression dataset analysis: 

GSEA reports : gsea_report_for_1_1645606100691 corresponding to SDY998 dataset, gsea_report_for_1_1644504102288 corresponding to GSE107011 dataset, gsea_report_for_1_1644503918451 corresponding to GSE135390 dataset, gsea_report_for_1_1644503609397 corresponding to GSE32901 dataset;

SDY998_seurat_object_T_cells.rds: a Seurat object corresponding to SDY998 dataset containing the CD4+ T cells; 

SDY998_cluster_identification_and_DEA : R script to perform the clustering and DEA on the SDY998 dataset;

SDY998_GSEA_overlap_between_gene_sets : R script to calculate the percentage of common genes between the enriched gene sets identified using SDY998; 

SDY998_GSEA_comparison_with_other_datasets: R script to keep only the gene sets identified using SDY998 dataset and at least one of the following datasets: GSE107011, GSE135390 and GSE32901; 

M1_M2_phenotype_assignment_to_RA_macrophage_pathways_array : R script to assign the M1 or M2 phenotype to the newly identified pathways stored in IPA selected pathways and metacore selected pathways;

IPApathwaySymbolList : GMT file used in the GSEA for the Th1 map enrichment; 

GSE97779_series_matrix : TXT file of the gene expression matrix of GSE97779 dataset containing the synovial macrophage samples; 

GSE97779_RA_macrophage: R script to perform the DEA on the GSE97779 dataset;

GSE164498_M1_M2_24h_human_DE_genes_Bioturing: TSV file containing the M1/M2 macrophage signatures we obtained using Bioturing software;

GSE109449_RA_fibroblast_for_RA_global_map_filtering: R script used to filter the RA map and keep only the fibroblast specific components;

GSE109449_RA_fibroblast_SC_DE_genes: TSV file containing the DEG list we obtained using GSE109449 dataset and bioturing software;

IPA selected pathway: folder containing the IPA identified pathways using GSE97779 gene expression dataset;

Metacore selected pathways: folder containing the Metacore identified pathways using GSE97779 gene expression dataset;

gsea_TH1_cluster_names_0_05_shared: folder containing the Th1 enriched and filtered gene sets.

